'use strict';
require('dotenv').config();
const connection = require("./db");
var express  = require('express'),
bodyParser   = require('body-parser'),
http         = require('http'),
server       = express(),
mongoose     = require('mongoose'),

Medicine     = require('./API/Models/Medicine');

connection();

server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json());

var routes = require('./API/Routes/Routes'); //importing route
routes(server); //register the route

server.listen((process.env.PORT || 8000), function () {
    console.log("Server is up and listening on port" + process.env.PORT);

});