var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Medicine = new Schema({

    name: {
        type: String,
        required: false
    },

    description: {
        type: String,
        required: false
    },

    scadenza: {
        type: String,
        required: false
    },

    interazioni: {
        type: String,
        required: false
    },

    precauzioni: {
        type: String,
        required: false
    },

    avvertenze: {
        type: String,
        required: false
    },

    sovradosaggio: {
        type: String,
        required: false
    },

    conservazione: {
        type: String,
        required: false
    },

    controindicazioni: {
        type: String,
        required: false
    },

    dosi: {
        type: String,
        required: false
    },

    effetti: {
        type: String,
        required: false
    }


},
    { versionKey: false, id: false, _id: false });

module.exports = mongoose.model('Medicine', Medicine);