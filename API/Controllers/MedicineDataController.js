'use strict';
var mongoose = require('mongoose');
const { WebhookClient, Payload } = require('dialogflow-fulfillment');  //Payload
var Medicine = mongoose.model('Medicine');
var medicine_item = {};
var resultItem = "";
var resultList = [];
var info = "";

exports.processRequest = function (req, res) {
  console.log("req.body.queryResult ### ", req.body.queryResult);

  if (req.body.queryResult.action == "indicazioni") {
    getMedicineInfo(req, res);
  }
  else if (req.body.queryResult.action == "cambia_attributo") {
    getAttributeInfo(req, res);
  }
  else if (req.body.queryResult.action == "elenco_medicinali") {
    getElencoMedicinali(req, res);
  }
};


function medicinePayload(agent) {
  var payloadData = {
    "richContent": [
      [
        {
          "type": "accordion",
          "title": medicine_item != undefined && medicine_item.name ? medicine_item.name : "Mi dispiace il farmaco non è presente",
          "subtitle": info != "" ? info : "Info",
          "text": medicine_item != undefined ? medicine_item[resultItem] : "Non conosco questo medicinale."
        }
      ]
    ]
  };
  agent.add(new Payload(agent.UNSPECIFIED, payloadData, { sendAsMessage: true, rawPayload: true }));
  if (medicine_item != undefined && medicine_item.name && medicine_item.name !== "Elenco medicinali") {
    let result = "Inoltre per il medicinale scelto " + medicine_item.name + ", possiamo fornirle altre informazioni : \navvertenze, \nsovradosaggio, \nscadenza e conservazione, \ncontroindicazioni, \nInterazioni, \nDosi e modi d'uso, \nEffetti indesiderati. \n \n Ora può scrivere l'informazione del farmaco di cui ha bisogno.";
    agent.add(result);
  }
}

function listPayload(agent) {
  var payloadData = { "richContent": [resultList] }
  agent.add(new Payload(agent.UNSPECIFIED, payloadData, { sendAsMessage: true, rawPayload: true }));
}

async function getMedicineInfo(req, res) {
  //Create an instance
  const agent = new WebhookClient({ request: req, response: res });
  var medicine = req.body.queryResult.parameters.medicine;
  info = "descrizione";
  if (req.body.queryResult.parameters.attributi !== "lista"){
    Medicine.find({ name: medicine }, { _id: 0 }, function (err, medicine) {
    if (err) {
      console.log("error find Medicine", err);
      return [{}];
    }
    if (medicine) {
      medicine_item = medicine[0];
      resultItem = "description";
      //console.log("medicine_item getMedicineInfo ", medicine_item);
      var intentMap = new Map();
      intentMap.set(req.body.queryResult.action, medicinePayload);
      agent.handleRequest(intentMap);
    }
  });
} else {
  let result = "Ero in attesa di un attributo per il medicinale. Se vuoi la lista, riscrivi il comando. Grazie";
    agent.add(result);
}
}

async function getElencoMedicinali(req, res) {
  //Create an instance
  const agent = new WebhookClient({ request: req, response: res });
  info = "Elenco medicinali";
  //console.log("parameters ###", eq.body.queryResult.parameters.medicine);

  if (req.body.queryResult.parameters.medicine !== "medicinali" && req.body.queryResult.parameters.medicine !== "medicine") {
    info = "descrizione";
    Medicine.find({ name: req.body.queryResult.parameters.medicine }, { _id: 0 }, function (err, medicine) {
      if (err) {
        console.log("error find Medicine", err);
        return [{}];
      }
      if (medicine) {
        medicine_item = medicine[0];
        resultItem = "description";
        //console.log("medicine_item getElencoMedicinali ", medicine_item);
        var intentMap = new Map();
        intentMap.set(req.body.queryResult.action, medicinePayload);
        agent.handleRequest(intentMap);
      }
    });
  }
  else {
    Medicine.find({}, function (err, availability) {
      if (err) {
        console.log("error find Medicine", err);
        return [{}];
      }
      medicine_item = {};
      medicine_item.name = "Elenco medicinali";
      resultList = [];
      //console.log("lista medicinali", availability);
      availability.forEach(element => {
        resultList.push({
          "type": "chips",
          "options": [
            {
              "text": element.name
            }
          ]
        })
      });
      var intentMap = new Map();
      intentMap.set(req.body.queryResult.action, listPayload);
      agent.handleRequest(intentMap);
    })
  }
}

async function getAttributeInfo(req, res) {
  const agent = new WebhookClient({ request: req, response: res });
  var medicineItem = req.body.queryResult.fulfillmentText;
  info = req.body.queryResult.parameters.attributi;
  Medicine.find({ name: medicineItem }, { _id: 0 }, function (err, medicine) {
    if (err) {
      console.log("error find Medicine", err);
      return [{}];
    }
    if (medicine) {
      medicine_item = medicine[0];
      resultItem = setAttribute(req.body.queryResult.parameters.attributi.toLowerCase());
      //console.log("medicine_item getAttributeInfo ", medicine_item, resultItem);
      var intentMap = new Map();
      intentMap.set(req.body.queryResult.action, medicinePayload);
      agent.handleRequest(intentMap);
    }
  });
}

function setAttribute(attribute) {
  console.log("attribute ###", attribute);
  if (attribute === "precauzioni per l'uso") {attribute = "precauzioni"}
  if (attribute === "scadenza e conservazione") {attribute = "scadenza"}
  if (attribute === "dosi e modi d'uso") {attribute = "dosi"}
  if (attribute === "effetti indesiderati") {attribute = "effetti"}
  return attribute;
}
