const mongoose = require("mongoose");

module.exports = async () => {
    try 
    {
        // mongoose instance connection url connection
        //mongoose.Promise = global.Promise;
        //mongoose.connect(config.dbUrl);
        const connectionParams = {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        };
        await mongoose.connect(process.env.MONGO_URI, connectionParams);
        //mongoose.connection.db.dropDatabase(); /* cancello il DB */
        console.log("connected to database.");
    } catch (error) {
        console.log("could not connect to database", error);
    }
};